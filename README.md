# Bundled Openssl for Windows Node apps

This is an example application for bundling openssl.exe with Node apps for use in Windows and the [pem](https://www.npmjs.com/package/pem) module.

## Download

```
git clone https://bitbucket.org/andris9/node-win32-openssl.git
cd node-win32-openssl
```

## Usage

```
npm install
npm test
```

If you see a private key and a certificate in the console output, then everything worked. See [index.js](index.js) for an actual usage example.

## NB!

Replace the bundled [cacert.pem](vendor/openssl/shared/cacert.pem) and [cakey.pem](vendor/openssl/shared/private/cakey.pem) in case you need the CA functionality.

You probably want to [download](http://indy.fulgan.com/SSL/) the latest openssl binaries as well instead of using (and trusting!) the bundled binaries from this repo. The bundled version is 1.0.1p.