'use strict';

var os = require('os');
var pem = require('pem');
var path = require('path');

if (process.platform === 'win32') {
    // use custom config, otherwise fallbacks to /usr/local/ssl/openssl.cnf
    process.env.OPENSSL_CONF = path.join(__dirname, 'vendor', 'openssl', 'shared', 'openssl.cnf');
    // use bundled openssl executable
    pem.config({
        pathOpenSSL: path.join(__dirname, 'vendor', 'openssl', os.arch() === 'x64' ? 'x64' : 'ia32', 'openssl.exe')
    });
}

pem.createCertificate({
    selfSigned: true
}, function(err, keys) {
    if (err) {
        console.log(err.stack);
        return;
    }
    console.log('%s\n\n%s', keys.serviceKey, keys.certificate);
});